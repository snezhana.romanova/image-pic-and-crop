// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../feature_splash/views/image_pick_page.dart';

class Routes {
  static const String imagePickPage = '/';
  static const all = <String>{
    imagePickPage,
  };
}

class MyRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.imagePickPage, page: ImagePickPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    ImagePickPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const ImagePickPage(),
        settings: data,
      );
    },
  };
}
