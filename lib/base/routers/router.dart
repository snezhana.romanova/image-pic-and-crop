import 'package:auto_route/auto_route_annotations.dart';
import '../../feature_splash/views/image_pick_page.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    MaterialRoute(page: ImagePickPage, initial: true),
  ],
)
class $MyRouter {}
