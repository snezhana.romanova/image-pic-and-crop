// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'image_pick_bloc.dart';

/// ImagePickBlocType class used for bloc event and state access from widgets
/// {@nodoc}
abstract class ImagePickBlocType extends RxBlocTypeBase {
  ImagePickBlocEvents get events;

  ImagePickBlocStates get states;
}

/// $ImagePickBloc class - extended by the ImagePickBloc bloc
/// {@nodoc}
abstract class $ImagePickBloc extends RxBlocBase
    implements ImagePickBlocEvents, ImagePickBlocStates, ImagePickBlocType {
  ///region Events

  ///region picImageFromCamera

  final _$picImageFromCameraEvent = PublishSubject<void>();
  @override
  void picImageFromCamera() => _$picImageFromCameraEvent.add(null);

  ///endregion picImageFromCamera

  ///region picImageFromGallery

  final _$picImageFromGalleryEvent = PublishSubject<void>();
  @override
  void picImageFromGallery() => _$picImageFromGalleryEvent.add(null);

  ///endregion picImageFromGallery

  ///endregion Events

  ///region States

  ///region getImage
  Stream<Image> _getImageState;

  @override
  Stream<Image> get getImage => _getImageState ??= _mapToGetImageState();

  Stream<Image> _mapToGetImageState();

  ///endregion getImage

  ///region cameraButton
  Stream<bool> _cameraButtonState;

  @override
  Stream<bool> get cameraButton =>
      _cameraButtonState ??= _mapToCameraButtonState();

  Stream<bool> _mapToCameraButtonState();

  ///endregion cameraButton

  ///region deviceButton
  Stream<bool> _deviceButtonState;

  @override
  Stream<bool> get deviceButton =>
      _deviceButtonState ??= _mapToDeviceButtonState();

  Stream<bool> _mapToDeviceButtonState();

  ///endregion deviceButton

  ///endregion States

  ///region Type

  @override
  ImagePickBlocEvents get events => this;

  @override
  ImagePickBlocStates get states => this;

  ///endregion Type

  /// Dispose of all the opened streams

  @override
  void dispose() {
    _$picImageFromCameraEvent.close();
    _$picImageFromGalleryEvent.close();
    super.dispose();
  }
}
