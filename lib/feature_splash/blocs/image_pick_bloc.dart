// used sample: https://github.com/Prime-Holding/rx_bloc/blob/master/examples/counter/lib/bloc/counter_bloc.dart

import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

part 'image_pick_bloc.rxb.g.dart';

abstract class ImagePickBlocEvents {
  void picImageFromCamera();
  void picImageFromGallery();
}

abstract class ImagePickBlocStates {
  Stream<Image> get getImage;
  Stream<bool> get cameraButton;
  Stream<bool> get deviceButton;
  //
  // @RxBlocIgnoreState()
  // Stream<bool> get onCropping;
  //
  @RxBlocIgnoreState()
  Stream<bool> get isLoading;
}

@RxBloc()
class ImagePickBloc extends $ImagePickBloc {

  ImagePickBloc() {
    MergeStream([
      _$picImageFromCameraEvent.map((_) => _getImage(ImageSource.camera)),
      _$picImageFromGalleryEvent.map((_) => _getImage(ImageSource.gallery))
    ]).bind(_selectedImage).disposedBy(_compositeSubscription);
  }

  // var _isCropping = false;
  final _selectedImage = BehaviorSubject();

  final _compositeSubscription = CompositeSubscription();

  _getImage(ImageSource source) async {
    // _isCropping = true;
    final image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      final cropped = await ImageCropper.cropImage(
        cropStyle: CropStyle.circle,
        sourcePath: image.path,
        aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
        compressQuality: 100,
        maxWidth: 700,
        maxHeight: 700,
        compressFormat: ImageCompressFormat.jpg,
        androidUiSettings: AndroidUiSettings(
          toolbarColor: Colors.deepOrange,
          toolbarTitle: 'RPS Cropper',
          statusBarColor: Colors.deepOrange.shade900,
          backgroundColor: Colors.white,
        ),
      );
      _selectedImage.value(cropped);
    }
    // _isCropping = false;
  }

  @override
  Stream<Image> _mapToGetImageState() {
    if (_selectedImage.value != null) {
      return _selectedImage.value.map((value) => Image.file(value));
    }
    return Stream<Image>.value(Image.asset('assets/images/place_holder.jpg'));
  }

  @override
  Stream<bool> _mapToCameraButtonState() {
    _$picImageFromCameraEvent.map((_) => _getImage(ImageSource.camera));
     return   _$picImageFromCameraEvent.map((_) => true);
  }

  @override
  Stream<bool> _mapToDeviceButtonState() {
    _$picImageFromGalleryEvent.map((_) => _getImage(ImageSource.gallery));
    return _$picImageFromGalleryEvent.map((_) => true);
  }

  @override
  Stream<bool> get isLoading => loadingState;

  @override
  void dispose() {
    _compositeSubscription.dispose();
    super.dispose();
  }

}
