import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:my_town_app/feature_splash/blocs/image_pick_bloc.dart';

class ImagePickPage extends StatelessWidget {
  const ImagePickPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Image picker'),
        ),
        body: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RxBlocBuilder<ImagePickBlocType, Image>(
                  state: (bloc) => bloc.states.getImage,
                  builder: (context, state, bloc) => Container(
                    width: 250,
                    height: 250,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: state.data.image,
                      ),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(180)),
                      color: Colors.redAccent,
                    ),
                  ),
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RxBlocBuilder<ImagePickBlocType, bool>(
                        state: (bloc) => bloc.states.cameraButton,
                        builder: (context, state, bloc) => RaisedButton(
                            color: Colors.green,
                            child: const Text(
                              'Camera',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: bloc.events.picImageFromCamera),
                      ),
                      RxBlocBuilder<ImagePickBlocType, bool>(
                          state: (bloc) => bloc.states.deviceButton,
                          builder: (context, state, bloc) => RaisedButton(
                              color: Colors.deepOrange,
                              child: const Text(
                                'Device',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: bloc.events.picImageFromGallery)),
                    ]),
              ],
            ),
            RxBlocBuilder<ImagePickBlocType, bool>(
                state: (bloc) => bloc.states.isLoading,
                builder: (context, state, bloc) => state.data ? Container(
                              color: Colors.white,
                              height: MediaQuery.of(context).size.height * 0.95,
                              child: const Center(
                                child: CircularProgressIndicator(),
                              ),
                            )
              : Container(),),
          ],
        ),
      );
}
